import {Component, OnDestroy} from '@angular/core';
import {NgRedux, select} from '@angular-redux/store';
import {IAppState} from './store';
import {CounterActions} from './app.actions';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'App works!';
  @select() readonly count: Observable<number>;
  constructor(
    private ngRedux: NgRedux<IAppState>,
    private action: CounterActions
  ) {
  }
  increment() {
    this.ngRedux.dispatch(this.action.increment());
  }
  decrement() {
    this.ngRedux.dispatch(this.action.decrement());
  }


}

